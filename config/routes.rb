Rails.application.routes.draw do
  root 'posts#index'

  resources :posts do
    resources :reviews
    resources :likes
  end
  resources :users

  get 'signup' => "users#new"
  get 'signin' => "sessions#new"
  resources :categories
  resource :session, only: [:new, :create, :destroy]
end
