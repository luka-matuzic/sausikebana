module LikesHelper
  def like_or_unlike_button(post, like)
    if like
      button_to "Unlike", post_like_path(post, like), method: :delete, class: "unlike-button"
    else
      button_to "Like", post_likes_path(post), class: "like-button"
    end
  end
end
