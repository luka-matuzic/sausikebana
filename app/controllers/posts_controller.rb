class PostsController < ApplicationController
  before_action :require_signin, except: [:index, :show]
  #before_action :require_admin, except: [:index, :show]
  before_action :set_post, only: [:show, :edit, :update, :destroy]

  def index
    @posts = Post.all.order("created_at DESC")
  end

  def show
    @likers = @post.likers
    @categories = @post.categories
    @badges = @post.badges

    if current_user
    @like = current_user.likes.find_by(post_id: @post.id)
    end
  end

  def edit
  end

  def update
    if @post.update(post_params)
      flash[:notice] = "Post successfully updated!"
      redirect_to post_path
    else
      render :edit
    end
  end

  def new
    @post = Post.new
  end

  def create
    @post = Post.new(post_params)
    if @post.save
      flash[:notice] = "Post successfully created!"
      redirect_to @post
    else
      render :new
    end 
  end

  def destroy
    @post.destroy
    flash[:alert] = "Post successfully deleted!"
    redirect_to root_path
  end


  private
    def post_params
      params.require(:post).permit(:title, :description, category_ids: [])
    end

    def set_post
      @post = Post.find_by!(slug: params[:id])
    end
end
