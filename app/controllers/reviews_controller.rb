class ReviewsController < ApplicationController
  before_action :require_signin
  before_action :set_post

  def index
    @reviews = @post.reviews.order("created_at DESC")
  end

  def new
    @review = @post.reviews.new
  end

  def create
    @review = @post.reviews.new(review_params)
    @review.user = current_user

    if @review.save
      redirect_to post_reviews_path(@post), notice: "Comment posted!"
    else
      render :new
    end
  end


  private 
    def review_params
      params.require(:review).permit(:comment, :like)
    end
    
    def set_post
      @post = Post.find_by!(slug: params[:post_id])
    end
end
