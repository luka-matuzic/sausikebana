class Badge < ApplicationRecord
  belongs_to :badgeable, polymorphic: true
end
