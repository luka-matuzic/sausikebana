class Post < ApplicationRecord
  before_save :set_slug

  has_many :reviews, dependent: :destroy
  has_many :likes, dependent: :destroy
  has_many :likers, through: :likes, source: :user
  has_many :categorizations, dependent: :destroy
  has_many :categories, through: :categorizations

  has_many :badges, as: :badgeable

  validates :title, presence: true, uniqueness: true
  validates :description, presence: true
  validates :title, :description, presence: true

  def to_param
    slug
  end

  private
    def set_slug
      self.slug = title.parameterize
    end
end
