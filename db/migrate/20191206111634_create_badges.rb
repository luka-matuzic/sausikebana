class CreateBadges < ActiveRecord::Migration[6.0]
  def change
    create_table :badges do |t|
      t.string :badgeable_type
      t.integer :badgeable_id
      t.integer :user_id
      t.text :body

      t.timestamps
    end
  end
end
